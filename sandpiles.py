#!/usr/bin/python
# coding=utf-8

import logging
import math


class c:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


########################################################

def header(string): return c.HEADER + string + c.ENDC

def blue(string): return c.OKBLUE + string + c.ENDC

def green(string): return c.OKGREEN + string + c.ENDC

def warning(string): return c.WARNING + string + c.ENDC

def error(string): return c.FAIL + string + c.ENDC

def bold(string): return c.BOLD + string + c.ENDC

########################################################

def full(rep=1): return '█'*rep

def empty(rep=1): return ' '*rep

########################################################


class Sandpile:

    SIZE = 15
    MAX_VALUE = 3

    CHARACTER = '█'


    def __init__(self):

        self.mat = [[0 for x in range(self.SIZE)] for y in range(self.SIZE)]


    def __str__(self):
        line = "\n"
        for x in range(self.SIZE):
            for y in range(self.SIZE):
                
                # if x == int(self.SIZE / 2) and y == int(self.SIZE / 2):
                #     line += green( "{:3d}".format(self.mat[x][y]) )
                # else:
                #     line += "{:3d}".format(self.mat[x][y])

                if self.mat[x][y] == 0:
                    # line += green('██')
                    line += ' '
                if self.mat[x][y] == 1:
                    line += green(self.CHARACTER)
                if self.mat[x][y] == 2:
                    line += error(self.CHARACTER)
                if self.mat[x][y] == 3:
                    line += blue(self.CHARACTER)
                    # line += '██'

            line += "\n"

        return line


    def increment(self, x, y, increment):
        self.mat[x][y] += increment


    def set(self, x, y, value):
        self.mat[x][y] = value


    def update(self):
        tempMat = self.mat

        for x in range(self.SIZE):
            for y in range(self.SIZE):

                if self.mat[x][y] > self.MAX_VALUE:

                    tempMat[x][y] = self.mat[x][y] - (self.MAX_VALUE + 1)

                    logging.debug("CURRENT: tempMat[{}][{}] = {}".format(x, y, tempMat[x][y]))

                    if x + 1 < self.SIZE:
                        tempMat[x+1][y] += 1
                        logging.debug("tempMat[{}][{}] = {}".format(x+1, y, tempMat[x+1][y]))
                    if x - 1 >= 0:
                        tempMat[x-1][y] += 1
                        logging.debug("tempMat[{}][{}] = {}".format(x-1, y, tempMat[x-1][y]))
                    if y + 1 < self.SIZE:
                        tempMat[x][y+1] += 1
                        logging.debug("tempMat[{}][{}] = {}".format(x, y+1, tempMat[x][y+1]))
                    if y - 1 >= 0:
                        tempMat[x][y-1] += 1
                        logging.debug("tempMat[{}][{}] = {}".format(x, y-1, tempMat[x][y-1]))

                    logging.info(self)
                    logging.debug("="*50)

        self.mat = tempMat


    def check(self):
        checkResult = True

        for x in range(self.SIZE):
            for y in range(self.SIZE):
                checkResult = (self.mat[x][y] <= self.MAX_VALUE) and checkResult

        return checkResult


def main():
    # logging.basicConfig(level=logging.DEBUG)

    sp = Sandpile()
    # for x in range(sp.SIZE):
    #     for y in range(sp.SIZE):
    #         sp.increment(x, y, 2)   
    
    center = int( math.floor(sp.SIZE / 2) )
    sp.increment(center, center, pow(2, 13) + 1)

    logging.info(sp)

    while not sp.check():
        sp.update()
        logging.info(sp)

    print sp


if __name__ == '__main__':
    main()
